import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fsmbasedtesting.fsm.FSM;
import fsmbasedtesting.fsm.FiniteState;
import fsmbasedtesting.ga.Chromosome;
import fsmbasedtesting.ga.Population;
import fsmbasedtesting.sst.SST;

public class App {
    public static void main(String[] args) {
    	// create states
    	FiniteState s1 = new FiniteState("s1");
    	FiniteState s2 = new FiniteState("s2");
    	FiniteState s3 = new FiniteState("s3");
    	FiniteState s4 = new FiniteState("s4");
    	FiniteState s5 = new FiniteState("s5");
    	
    	// add transitions with Input & Output
    	s1.addTransition(s1, 'a', 'x');
    	s1.addTransition(s2, 'b', 'x');
    	s1.addTransition(s4, 'c', 'y');
    	
    	s2.addTransition(s3, 'b', 'y');
    	s2.addTransition(s5, 'a', 'x');
    	
    	s3.addTransition(s5, 'b', 'x');
    	s3.addTransition(s5, 'c', 'y');
		
    	s4.addTransition(s5, 'a', 'x');
    	s4.addTransition(s3, 'b', 'x');
		
    	
    	s5.addTransition(s1, 'c', 'z');
    	s5.addTransition(s4, 'a', 'z');
    	
    	// set final state
    	s5.setFinal();
    	
    	// create new FSM based on s1 transitions
    	FSM M = new FSM(s1, '#');
    	
    	M.states.add(s1);
    	M.states.add(s2);
    	M.states.add(s3);
    	M.states.add(s4);
    	M.states.add(s5);
    	
    	// GA
    	
		// The size of the simulation population
		final int populationSize = 30;
		
		// The maximum number of generations for the simulation.
		final int maxGenerations = 250;
		
		// The probability of crossover for any member of the population,
		// where 0.0 <= crossoverRatio <= 1.0
		final float crossoverRatio = 0.75f;
		
		// The portion of the population that will be retained without change
		// between evolutions, where 0.0 <= elitismRatio < 1.0
		final float elitismRatio = 0.0f;
		
		// The probability of mutation for any member of the population,
		// where 0.0 <= mutationRatio <= 1.0
		final float mutationRatio = 0.1f;
	
		// Get the current run time.  Not very accurate, but useful for 
		// some simple reporting.
		long startTime = System.currentTimeMillis();
		
		final int solutionSize = 10;
		
		// Create the initial population
		
    	List<Character> language_input = new ArrayList<Character>();
    	
    	language_input.add('a');
    	language_input.add('b');
    	language_input.add('c');
    	//language_input.add('#');
    	
		Population pop = new Population(populationSize, crossoverRatio, 
				elitismRatio, mutationRatio, language_input, solutionSize, M);

		// Start evolving the population, stopping when the maximum number of
		// generations is reached, or when we find a solution.
		int i = 0;
		Chromosome best = pop.getPopulation()[0];
		
		while ((i++ < maxGenerations) && (best.getFitness() != 0)) {
			System.out.println("Generation " + i + ": " + best.getGene());
			System.out.println("UIO" + best.getUIO());
			pop.evolve();
			best = pop.getPopulation()[0];
		}
		
		System.out.println(pop.getPopulation()[0].getFitness());
		
		// Get the end time for the simulation.
		long endTime = System.currentTimeMillis();
		

		System.out.println("Total execution time: " + (endTime - startTime) + 
				"ms");
    	/*
    	
		SST<List<String>> root = new SST<List<String>>(new ArrayList<String>(Arrays.asList("S1","S2","S3","S4","S5","S6")) , null, true);
    	{
    		
    		root.hasParent();
    		
    		SST<List<String>> node1 = root.addChild(new ArrayList<String>(Arrays.asList("S1,S3,S5")), "a/x");
    		
    		SST<List<String>> node2 = root.addChild(new ArrayList<String>(Arrays.asList("S2,S4,S6")), "a/y");
    		
    		SST<List<String>> node3 = node1.addChild(new ArrayList<String>(Arrays.asList("S1")), "b/x");
    		SST<List<String>> node4 = node1.addChild(new ArrayList<String>(Arrays.asList("S3,S5")), "b/y");
    		SST<List<String>> node5 = node2.addChild(new ArrayList<String>(Arrays.asList("S2,S4")), "b/x");
    		SST<List<String>> node6 = node2.addChild(new ArrayList<String>(Arrays.asList("S6")), "b/y");
    		
    		SST<List<String>> node7 = node4.addChild(new ArrayList<String>(Arrays.asList("S3")), "a/x");
    		SST<List<String>> node8 = node4.addChild(new ArrayList<String>(Arrays.asList("S5")), "a/y");
    		SST<List<String>> node9 = node5.addChild(new ArrayList<String>(Arrays.asList("S2")), "b/x");
    		SST<List<String>> node10 = node5.addChild(new ArrayList<String>(Arrays.asList("S4")), "b/y");
    		
    		
    		root.generateUIO(root);
    		System.out.println(root.getUIO());
    		
    		//root.print(root, 1);
    	}
    	*/
    }
}

package fsmbasedtesting.sst;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SST<T> implements Iterable<SST<T>> {

	List<T> data;
	public SST<T> parent;
	List<SST<T>> children;
	String edge_text;
	boolean is_viewed;
	public Map<String, List<String>> uio = new HashMap<String, List<String>>();

	public SST(List<T> data, String edge_text, boolean is_viewed) {
		this.data = data;
		this.edge_text = edge_text;
		this.children = new LinkedList<SST<T>>();
		this.is_viewed = is_viewed;
	}

	
	public void addChild(SST<T> child, String edge_text) {
		child.parent = this;
		this.children.add(child);
	}

	public boolean isLeaf() {
		return this.children.size() == 0 && this.data.size() == 1;
	}

	@Override
	public Iterator<SST<T>> iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void setParent(SST<T> parent, String edge_text) {
		this.parent = parent;
		this.edge_text = edge_text;
	}

	public String getEdgeText() {
		return this.edge_text;
	}

	public void setIsViewed() {
		this.is_viewed = true;
	}

	public SST<T> getParent() {
		return this.parent;
	}

	public List<T> getData() {
		return this.data;
	}

	public List<SST<T>> getChildren() {
		return this.children;
	}

	public boolean isViewed() {
		return this.is_viewed;
	}

	public boolean hasChild() {
		if (children.size() == 0) {
			return false;
		}

		if (this.is_viewed) {
			return false;
		}

		return true;
	}

	public boolean hasParent() {
		if (this.parent == null) {
			return false;
		}

		return true;
	}
	
	public Map<String, List<String>> getUIO() {
		return this.uio;
	}

	public int getDiscretePartitions(SST<String> root) {
		return getLeaf(root);
	}

	public int getGroups(SST<String> root) {
		int sum = 0;
		for (SST<String> node : root.getChildren()) {
			sum += getGroups(node);
		}
		if (root.getData().size() > 1) {
			return root.getData().size();
		}

		return sum;
	}

	private int getLeaf(SST<String> root) {
		int sum = 0;
		for (SST<String> node : root.getChildren()) {
			if (!node.isLeaf()) {
				sum += getLeaf(node);
			}
		}

		if (root.isLeaf()) {
			return 1;
		}

		return sum;
	}
	

    public void generateUIO(SST<String> root) {
	//Map<String, List<String>> uio = new HashMap<String, List<String>>();
	
	for(SST<String> node : root.getChildren()) {
		if(!node.isViewed()) {
			generateUIO(node);
		}
	}
	
	if(root.isLeaf()) {
		if(getSSTNodeParents(root).size() > 0) {
			String key = new String();
			
			for(int i = 0; i < root.getData().size(); i++) {
				key += root.getData().get(i) + " ";
			}

			this.uio.put(key, getSSTNodeParents(root));
		}

	}
	
	root.setIsViewed();
		
	}


	public static List<String> getSSTNodeParents(SST<String> node) {
		SST<String> temp_node = node;
		List<String> uio = new ArrayList<String>();
	
		String last_uio;
		String[] last_uio_parts;
		String[] current_uio_parts;
		String uio_text;
		
		while(temp_node.hasParent()) {
			if(uio.size() == 0) {
				uio.add(temp_node.getEdgeText());
			} else {
				last_uio = uio.get(uio.size() -1);
				last_uio_parts = last_uio.split("/");
				current_uio_parts = temp_node.getEdgeText().split("/");
				uio_text = last_uio_parts[0] + current_uio_parts[0] + "/" + last_uio_parts[1] + current_uio_parts[1];
					
				uio.add(uio_text);
				
			}
			
			temp_node = temp_node.getParent();
		}	
		
		return uio;
	}

	public void print(SST<T> root, int i) {
		System.out.println("LEVEL "+i);
		System.out.println(root.getData());
			for(SST<T> children : root.getChildren()) {
				print(children, i + 1);
			}
		
	}
	
	private void printChildren(SST<T> root) {
		for(SST<T> children_node : root.getChildren()) {
			System.out.println(children_node.getData());
		}
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((children == null) ? 0 : children.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((edge_text == null) ? 0 : edge_text.hashCode());
		result = prime * result + (is_viewed ? 1231 : 1237);
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		result = prime * result + ((uio == null) ? 0 : uio.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SST other = (SST) obj;
		if (children == null) {
			if (other.children != null)
				return false;
		} else if (!children.equals(other.children))
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (edge_text == null) {
			if (other.edge_text != null)
				return false;
		} else if (!edge_text.equals(other.edge_text))
			return false;
		if (is_viewed != other.is_viewed)
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		if (uio == null) {
			if (other.uio != null)
				return false;
		} else if (!uio.equals(other.uio))
			return false;
		return true;
	}
	
	
}
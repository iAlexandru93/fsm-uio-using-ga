package fsmbasedtesting.fsm;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FiniteState extends State {
	public static final FiniteState STATE_NULL = new FiniteState("null");
	
	private boolean isFinal = false;
	private List<FiniteTransition> transitions;
	
	public FiniteState(String name) {
		super(name);
		this.transitions = new LinkedList<FiniteTransition>();
	}
	
	public void setFinal() {
		this.isFinal = true;
	}
	
	public boolean isFinal() {
		return this.isFinal;
	}
	
	public FiniteTransition getTransition(FiniteState state) {
		for (FiniteTransition current_transition : this.transitions) {
			if(current_transition.getNextState() == state) {
				return current_transition;
			}
		}
		
		return new FiniteTransition();
	}
	
	public List<FiniteTransition> getTransitions( char input) {
		List<FiniteTransition> result = new ArrayList<FiniteTransition>();
		
		for(FiniteTransition current_transition : this.getTransitions()) {
			if(current_transition.getInput() == input) {
				result.add(current_transition);
			}
		}
		return result;
	}
	
	public FiniteState addTransition(FiniteState state, char input, char output) {
		FiniteTransition transition = new FiniteTransition(this, state, input, output);
		
		if (transitions.contains(transition)) {
			return this;
		}
		
		transitions.add(transition);
		return this;
	}
	
	public List<FiniteState> getState(char c) {
		List<FiniteState> states = new LinkedList<FiniteState>();
		for (FiniteTransition transition : this.transitions) {
			if (transition.getInput() == c) {
				states.add(transition.getNextState());
			}
		}
		
		return states;
	}
	
	public List<FiniteState> getStates() {		
		List<FiniteState> states = new ArrayList<FiniteState>();
		
		for(FiniteTransition transition : this.getTransitions()) {
			states.add(transition.getNextState());
		}
		
		return states;
	}
	
	public List<FiniteTransition> getTransitions() {
		return this.transitions;
	}
	
	@Override
	public String toString() {
		return super.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (isFinal ? 1231 : 1237);
		result = prime * result + (super.hashCode());
		result = prime * result + ((transitions == null) ? 0 : transitions.hashCode());
		return result;
	}
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof FiniteState)) {
			return false;
		}
		FiniteState other = (FiniteState) obj;
		if (isFinal != other.isFinal) {
			return false;
		}
		if (!super.equals(other)) {
			return false;
		}
		if (transitions == null) {
			if (other.transitions != null) {
				return false;
			}
		} else if (!transitions.equals(other.transitions)) {
			return false;
		}
		return true;
	}
}

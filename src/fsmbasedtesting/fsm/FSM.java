package fsmbasedtesting.fsm;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class FSM {
	private FiniteState initial;
	
	public String UIO;
	
	public char doNotCare;
	
	public List<FiniteState> states =  new ArrayList<FiniteState>();
	
	public FSM(FiniteState initial, char doNotCare) {
		this.initial = initial;
		this.doNotCare = doNotCare;
	}
	
	public FiniteState getInitial() {
		return this.initial;
	}
	
	public List<FiniteState> getStates() {
		return this.states;
	}
	
	public void testInput(String input) {
		
	}
	
	public FiniteState getStateByName(String name) {
		for(FiniteState state : states)
			if(state.toString().toLowerCase() == name.toLowerCase())
				return state;
		return null; 
	}
	public Result testWord(final String word) {
		return testWord(getInitial(), word, 0, new Result(word));
	}
	
	private Result testWord(final FiniteState state, final String word, final Integer index, final Result result) {
		result.addState(state);

		if (index >= word.length()) {
			return result;
		}
		
		final List<FiniteState> states = state.getState(word.charAt(index));
		Result nextResult = null;
		
		// check for do_not_care char
		if(word.charAt(index) == this.doNotCare) {
			return testWord(state, word, index + 1, result.clone());
		}
		
		for (FiniteState next : states) {
			nextResult = testWord(next, word, index+1, result.clone());
			
			nextResult.addToOutput(state.getTransition(next).getOutput());		
			nextResult.addToInput(state.getTransition(next).getInput());
			
			if (nextResult != null && nextResult.isValid()) {	
				return nextResult;
			}
		}
		
		return nextResult;
	}
	
	public class Result implements Cloneable {
		private String word;
		private List<FiniteState> states;
		private List<Character> input = new ArrayList<Character>();
		private List<Character> output = new ArrayList<Character>();

		public Result(final String word) {
			this.word = word;
			this.states = new LinkedList<FiniteState>();
		}
		
		public void addToInput(char test) {
			this.input.add(test);
		}
		
		public void addToOutput(char test) {
			this.output.add(test);
		}

		public Boolean isValid() {
			return states.get(states.size()-1).isFinal();
		}

		public FiniteState addState(FiniteState state) {
			this.states.add(state);
			return state;
		}
		
		public List<FiniteState> getStates() {
			return states;
		}

		public String getWord() {
			return word;
		}
		
		@Override
		public Result clone() {
			Result result = new Result(getWord());
			for (FiniteState state : states) {
				result.addState(state);
			}
			return result;
		}
		
		public String toString() {
			StringBuffer string = new StringBuffer();
			for (int i = 0; i < word.length(); i++) {
				string.append("[");
				string.append(states.get(i).getName());
				string.append("]");
				string.append(" -- ");
				string.append(word.charAt(i));
				string.append(" --> ");
			}
			
			string.append("[");
			string.append(states.get(word.length()).getName());
			string.append("]");
			
			return string.toString();
		}
		
		public void printUIO() {
			System.out.println(this.input.toString());
			
			// reverse O
			List<Character> output_reverse = new ArrayList<Character>();

			for(int i = this.output.size() - 1; i >=0; i--) {
				output_reverse.add(this.output.get(i));
			}
			
			
			System.out.println(output_reverse.toString());
		}
	}
}
package fsmbasedtesting.fsm;

public class FiniteTransition {
	private FiniteState current_node;
	private FiniteState next_node;
	private char input;
	private char output;
	
	public FiniteTransition(FiniteState current_node, FiniteState next_node, final char input, final char output) {
		super();
		this.next_node = next_node;
		this.current_node = current_node;
		this.input = input;
		this.output = output;
	}
	
	public FiniteTransition() {
		this.next_node = FiniteState.STATE_NULL;
		this.current_node = FiniteState.STATE_NULL;
		this.input = '@';
		this.output = '@';
	}

	public FiniteState getCurrentState() {
		return this.current_node;
	}
	
	public FiniteState getNextState() {
		return this.next_node;
	}

	public char getInput() {
		return input;
	}
	
	public char getOutput() {
		return output;
	}

	@Override
	public String toString() {
		return "FiniteTransition [current_node=" + current_node + ", next_node=" + next_node + ", input=" + input
				+ ", output=" + output + "]";
	}




}
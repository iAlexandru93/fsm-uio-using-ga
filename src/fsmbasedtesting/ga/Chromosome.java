package fsmbasedtesting.ga;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import fsmbasedtesting.fsm.FSM;
import fsmbasedtesting.fsm.FiniteState;
import fsmbasedtesting.fsm.FiniteTransition;
import fsmbasedtesting.sst.SST;

public class Chromosome implements Comparable<Chromosome> {
	private final String gene;
	private double fitness;
	
	private int iterations = 0;
	
	private Map<String, List<String>> UIO = new HashMap<String, List<String>>();

	/** Convenience randomizer. */
	private static final Random rand = new Random(System.currentTimeMillis());

	/**
	 * Default constructor.
	 *
	 * @param gene
	 *            The gene representing this <code>Chromosome</code>.
	 */
	public Chromosome(String gene, FSM M) {
		this.gene = gene;
		this.fitness = calculateFitness(gene, M);
	}

	/**
	 * Method to retrieve the gene for this <code>Chromosome</code>.
	 *
	 * @return The gene for this <code>Chromosome</code>.
	 */
	public String getGene() {
		return gene;
	}

	/**
	 * Method to retrieve the fitness of this <code>Chromosome</code>. Note that a
	 * lower fitness indicates a better <code>Chromosome</code> for the solution.
	 *
	 * @return The fitness of this <code>Chromosome</code>.
	 */
	public double getFitness() {
		return fitness;
	}

	public Map<String, List<String>> getUIO() {
		return UIO;
	}

	/**
	 * Helper method used to calculate the fitness for a given gene. The fitness is
	 * defined as being the sum of the absolute value of the difference between the
	 * current gene and the target gene.
	 * 
	 * @param gene
	 *            The gene to calculate the fitness for.
	 * 
	 * @return The calculated fitness of the given gene.
	 */
	private double calculateFitness(String sequence, FSM M) {
		// System.out.println("NEW FITNESS");
		
		double F = 0;
		List<FiniteState> states = M.states;
		List<String> root_text = new ArrayList<String>();

		// Creating the root node containing all states
		for (FiniteState state : states) {
			root_text.add(state.getName());
		}

		// Initialize a new SST with the root node
		SST<String> root_sst = new SST<String>(root_text, null, true);


		//fitness = SST_traversal(root_sst, M, sequence, 0, 0.0)/ sequence.length();
		this.fitness = 0;
		//System.out.println(" SEQ:" + sequence);
		
		//System.out.println(sequence);
		SST_traversal(root_sst, root_sst, M, sequence, 0);
		
		this.fitness = this.fitness / sequence.length();
		
		//System.out.println(" SEQ:" + sequence);
		//System.out.println(" Print SST:");
		//root_sst.print(root_sst, 1);
		//System.out.println(" END OF PRINT SST");
		
		//F = this.fitness / sequence.length();
		
		//System.out.println("FITNESS: " + this.fitness);
		// fitness = ThreadLocalRandom.current().nextDouble(0.3, 0.8);
		//System.out.println("NEW TREE");
		
		root_sst.generateUIO(root_sst);
		//System.out.println("NEW UIO");
		//System.out.println(root_sst.getUIO());
		
		// System.out.println(root_sst.getUIOBySTT());
		this.UIO = root_sst.getUIO();

		//System.out.println("F: "+this.fitness);
	
		return this.fitness;
	}

	private double SST_traversal(SST<String> original_root, SST<String> root , FSM M, String sequence, int i) {
		if(root.getParent() != null) {
			//System.out.println("PARENT: "+root.parent.getData());
			//root.setParent(original_root, "ASD/ASD");
		}
		if(i > sequence.length() - 1) {
			return 0;
		}
		
		if(sequence.charAt(i) == '#') {
			return SST_traversal(original_root, root, M, sequence, i+1);
		}
		
		//System.out.println("CURRENT ROOT" + root.getData());
		
		//System.out.println("NODE "+original_root.getData());
		
		for (SST<String> current_node : original_root.getChildren()) {
			//System.out.println("CHILD " +current_node.getData());
		}
		
		int x = original_root.getDiscretePartitions(root); // number of existing discrete partitions
		int dx = 0; // number of discrete partitions caused by the i th input
		int y = original_root.getGroups(root); // number of existing separated groups
		int dy = 0; // number of new groups
		String seq_without_do_not_care = sequence.replaceAll("#", "");
		//System.out.println("SEQ " + seq_without_do_not_care.substring(0, i));

		int max_length_seq = i;
		
		if(i > seq_without_do_not_care.length() - 1) {
			max_length_seq = seq_without_do_not_care.length();
		} else {
			max_length_seq = seq_without_do_not_care.substring(0, i).length();
		}
		
		int l = max_length_seq;
		
		if(l == 0) {
			l = 1;
		}

		if (i <= sequence.length() - 1) {
			List<SST<String>> result = buildSST(root.getData(), M, sequence.charAt(i));
			for (SST<String> current_node : result) {
				//System.out.println("ROOT PARENT" + root.getData());
				//System.out.println("NEW CHILD: "+ current_node.getData());
				
				current_node.parent = root;
				if(current_node.getData().hashCode() != root.getData().hashCode()) {
					//System.out.println(current_node.getData() + " /// " + root.getData());
					root.addChild(current_node, current_node.getEdgeText());
				}
				
				//current_node.setParent(root, current_node.getEdgeText());
				//if (!root.isLeaf())
					this.fitness += SST_traversal(original_root,current_node, M, sequence, i + 1);
			}
			dx = root.getDiscretePartitions(root) + x;
			dy = root.getGroups(root) + y;
		}
		
		
		// System.out.println(" X: " + x + ", Y: " + y + ", DX:" + dx + ", DY:" + dy);
		this.fitness += calculateFitness(x, y, dx, dy, l, Population.ALPHA, Population.BETA, Population.GAMMA, i);
		return 0;
		//return calculatedFitnessForIteration;
	}

	private double calculateFitness(int x, int y, int dx, int dy, int l, double alpha, double beta, double gamma,
			int index) {
		double first = 0;
		double second = 0;

		try {
			first = ((x * Math.pow(Math.E, x + dx)) / Math.pow(l, gamma));
			second = ((y + dy) / l);
			//System.out.println(first + " " + second + " /// beta " + Population.BETA +" /////"+ (y + dy ) + " / " + " l" + l);
			//System.out.println("DEBUG -> X: " + x + ", Y: " + y + ", DX:" + dx + ", DY:" + dy + " - L :" + l);
			//System.out.println("f(" + index + ") : " + alpha * first + beta * second);
			return alpha * first + beta * second;
		} catch (ArithmeticException e) {
			System.out.println("alfa * " + first + " beta * " + second);
			System.out.println("DEBUG -> X: " + x + ", Y: " + y + ", DX:" + dx + ", DY:" + dy + " - L :" + l);
		}
		return 0;
	}

	// Builds the SST
	private List<SST<String>> buildSST(List<String> data, FSM M, char input_char) {
		Map<String, List<FiniteState>> possible_nodes = getPossibleWays(data, M, input_char);
		
		List<SST<String>> toReturn = new ArrayList<SST<String>>();
		for (String c : possible_nodes.keySet()) {
			List<String> temp = new ArrayList<String>();
			for (FiniteState x : possible_nodes.get(c)) {
				temp.add(x.toString());
			}

			toReturn.add(new SST<String>(temp, c, false));
		}

		return toReturn;
	}

	// Method that should check with the FSM the possible transitions from all the
	// states
	private Map<String, List<FiniteState>> getPossibleWays(List<String> data, FSM M, char input_char) {
		// convert states from String to FiniteState
		List<FiniteState> myStates = new ArrayList<FiniteState>();
		

		for (String state : data) {
			myStates.add(M.getStateByName(state));
		}

		Map<String, List<FiniteState>> mapped_transitions = new HashMap<String, List<FiniteState>>();
		for (FiniteState state : myStates) {
			//System.out.println(" CHAR " + input_char);
			List<FiniteTransition> possibleTransitions = state.getTransitions(input_char);
			for (FiniteTransition transition : possibleTransitions) {
				List<FiniteState> temp = mapped_transitions.get(transition.getInput() + "/" + transition.getOutput());
				//System.out.println("NEW TRANS "+ transition.getInput() + " / " + transition.getOutput());
				
				if (temp == null)
					temp = new ArrayList<FiniteState>();
				temp.add(transition.getCurrentState());
				mapped_transitions.put(transition.getInput() + "/" + transition.getOutput(), temp);
			}
		}

		return mapped_transitions;
	}

	/**
	 * Method to generate a new <code>Chromosome</code> that is a random mutation of
	 * this <code>Chromosome</code>. This method randomly selects one character in
	 * the <code>Chromosome</code>s gene, then replaces it with another random (but
	 * valid) character. Note that this method returns a new
	 * <code>Chromosome</code>, it does not modify the existing
	 * <code>Chromosome</code>.
	 * 
	 * @return A mutated version of this <code>Chromosome</code>.
	 */
	public Chromosome mutate(List<Character> language_input, FSM M) {
		char[] arr = gene.toCharArray();

		List<Character> temp_language_input = new ArrayList<Character>(language_input);
		int replace_char_idx = rand.nextInt(language_input.size() - 1);

		Random rand = new Random();

		// for (int i = 0; i < arr.length; i++) {
		int randomElement = temp_language_input.get(rand.nextInt(temp_language_input.size()));
		arr[replace_char_idx] = (char) randomElement;
		// }

		return new Chromosome(String.valueOf(arr), M);
	}

	/**
	 * Method used to mate this <code>Chromosome</code> with another. The resulting
	 * child <code>Chromosome</code>s are returned.
	 * 
	 * @param mate
	 *            The <code>Chromosome</code> to mate with.
	 * 
	 * @return The resulting <code>Chromosome</code> children.
	 */
	public Chromosome[] mate(Chromosome mate, FSM M) {
		// Convert the genes to arrays to make thing easier.
		char[] arr1 = gene.toCharArray();
		char[] arr2 = mate.gene.toCharArray();

		// Select a random pivot point for the mating
		int pivot = rand.nextInt(arr1.length);

		// Provide a container for the child gene data
		char[] child1 = new char[gene.length()];
		char[] child2 = new char[gene.length()];

		// Copy the data from each gene to the first child.
		System.arraycopy(arr1, 0, child1, 0, pivot);
		System.arraycopy(arr2, pivot, child1, pivot, (child1.length - pivot));

		// Repeat for the second child, but in reverse order.
		System.arraycopy(arr2, 0, child2, 0, pivot);
		System.arraycopy(arr1, pivot, child2, pivot, (child2.length - pivot));

		return new Chromosome[] { new Chromosome(String.valueOf(child1), M),
				new Chromosome(String.valueOf(child2), M) };
	}

	/**
	 * A convenience method to generate a randome <code>Chromosome</code>.
	 * 
	 * @return A randomly generated <code>Chromosome</code>.
	 */
	/* package */ static Chromosome generateRandom(List<Character> language_input, int solutionSize, FSM M) {
		char[] arr = new char[solutionSize];
		Random rand = new Random();

		for (int i = 0; i < arr.length; i++) {
			int randomElement = language_input.get(rand.nextInt(language_input.size()));
			arr[i] = (char) randomElement;
		}

		return new Chromosome(String.valueOf(arr), M);
	}

	/**
	 * Method to allow for comparing <code>Chromosome</code> objects with one
	 * another based on fitness. <code>Chromosome</code> ordering is based on the
	 * natural ordering of the fitnesses of the <code>Chromosome</code>s.
	 */
	@Override
	public int compareTo(Chromosome c) {
		//System.out.println(fitness + " - " + c.fitness);
		if (fitness < c.fitness) {
			return -1;
		} else if (fitness > c.fitness) {
			return 1;
		}

		return 0;
	}

	/**
	 * @see Object#equals(Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Chromosome)) {
			return false;
		}

		Chromosome c = (Chromosome) o;
		return (gene.equals(c.gene) && fitness == c.fitness);
	}

	/**
	 * @see Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return new StringBuilder().append(gene).append(fitness).toString().hashCode();
	}
}
